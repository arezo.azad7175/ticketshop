<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users\BuyController;
use App\Http\Controllers\Admins\PageController;
use App\Http\Controllers\Users\UserCancelTicket;
use App\Http\Controllers\Users\PaymentController;
use App\Http\Controllers\Users\UserPagesController;
use App\Http\Controllers\Admins\AddTicketController;
use App\Http\Controllers\Admins\DashboardController;
use App\Http\Controllers\Users\ExportOrderController;
use App\Http\Controllers\Admins\ShowTicketsController;
use App\Http\Controllers\Users\Auth\UserLoginController;
use App\Http\Controllers\Users\Auth\UserLogoutController;
use App\Http\Controllers\Users\BuyTicketSearchController;
use App\Http\Controllers\Admins\Auth\AdminLoginController;
use App\Http\Controllers\Users\UserSearchTicketController;
use App\Http\Controllers\Admins\Auth\AdminLogoutController;
use App\Http\Controllers\Admins\Auth\AdminRegisterController;
use App\Http\Controllers\Users\SingleOrderedTicketsController;
use App\Http\Controllers\Admins\Auth\AdminForgetPassController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Admin

Route::group(['middleware' => 'guest:admin'],function()
{
    // login    
    Route::get('/' , [PageController::class , 'adminLoginShow'])->name('admin.login');
    Route::post('/adminLogin' , [AdminLoginController::class , 'adminLogin'])->name('adminLogin.post');

    // register
    Route::get('/adminRegister' , [PageController::class , 'adminRegisterShow'])->name('admin.Register');
    Route::post('/adminRegister' , [AdminRegisterController::class , 'adminRegister'])->name('adminRegister.post');

    // forgetpass
    Route::get('/adminForgetPass' , [PageController::class , 'adminForgetPassShow'])->name('forgetPass.get');
    Route::post('/adminForgetPass' , [AdminForgetPassController::class , 'setNewPass'])->name('forgetPass.post');

});


Route::group(['middleware' => 'auth.admin'],function()
{
  // welcom page
  Route::get('/welcom' , [PageController::class ,'showWelcom'])->name('welcom');

  // logout
  Route::post('/adminLogout' , [AdminLogoutController::class , 'adminlogout'])->name('adminlogout');

  // dashboard
  Route::get('/dashboard' , [PageController::class ,'showDashboard'])->name('dashboard');
  Route::get('/dashboardValue' , [DashboardController::class ,'dashboardValue'])->name('dashboardValue');

  Route::prefix('ticket')->group(function(){
    
    // add ticket
    Route::get('/addTicket' , [PageController::class ,'showAddTicket'])->name('addTicket');
    Route::post('/addTicket' , [AddTicketController::class ,'addTicket'])->name('addTicket.post');

    // show tickets
    Route::get('/showTickets' , [ShowTicketsController::class ,'showTickets'])->name('showTickets');
    Route::get('/tickets/{id}' , [ShowTicketsController::class ,'destroyTicket'])->name('destroyTicket');
    Route::post('/showTickets' , [ShowTicketsController::class ,'searchTickets'])->name('searchTickets');
      
  });

  // show_Users_Info
  Route::get('/users' ,  [PageController::class ,'showUsersInfo'])->name('users.info');

});


// User

Route::group(['middleware' => 'guest:web'],function()
{
  // User_login
  Route::get('/safir' , [UserPagesController::class , 'userLoginShow'])->name('user.login');
  Route::post('/userLogin' , [UserLoginController::class , 'userLogin'])->name('userLogin.post');
  
  // enter_pass_user
  Route::get('/safir/pass' , [UserPagesController::class , 'userPassShow'])->name('user.pass');
  Route::post('/pass-user/{mobile}' , [UserLoginController::class , 'checkUserPass'])->name('userPass.post');

  // payment
  Route::any('payment-verify' , [PaymentController::class , 'verify'])->name('payment.verify');
});


Route::group(['middleware' => 'auth.user'],function()
{

   
  // payment
  Route::get('payment' , [PaymentController::class , 'create'])->name('payment.create');
 
  // User_buy ticket
  Route::get('/buyTicket' , [UserPagesController::class ,'buyTicketShow'])->name('buyTicket');

  // BuyTicketSearch
  Route::post('/buyTicketSearch' , [BuyTicketSearchController::class ,'buyTicketSearch'])->name('buyTicketSearch');
  Route::post('/buy/{id}' , [BuyController::class ,'buy'])->name('buy');

  // logout
  Route::post('/useLogout' , [UserLogoutController::class , 'userlogout'])->name('userlogout');

  // search_ticket
  Route::get('/search/Ticket' , [UserPagesController::class ,'showSearchTicket'])->name('searchTicket');
  Route::post('/search/orderTicket' , [UserSearchTicketController::class , 'searchTicket'])->name('search.OrderTicket');

  // cancel_order
  Route::get('/cancel/order/{order_id}' , [UserCancelTicket::class , 'cancelTicket'])->name('cancelTicket');

  // export_pdf_order
  Route::get('/exportOrderPdf/{order_id}' , [ExportOrderController::class ,'exportOrder'])->name('exportOrder');

  // show_bought_tickets
  Route::get('/orderedTickets' , [UserPagesController::class , 'showOrderedTickets'])->name('orderedTickets');
  Route::get('/singleOrderedTickets/{order_id}' , [SingleOrderedTicketsController::class , 'showOrderedTickets'])->name('SingleOrderedTickets');
});





