<?php

namespace App\Models;

use App\Models\User;
use App\Models\Ticket;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'ticket_id',
        'count',
        'amount',
        'tracking_code',
        'payment',
    ];
    
    public function user():BelongsTo
    {
        return $this->belongsTo(User::Class);
    }

    public function ticket():BelongsTo
    {
        return $this->belongsTo(Ticket::class);
    }
}
