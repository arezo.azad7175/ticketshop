<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'firstName'=>'bail|required|string',
            'lastName'=>'bail|required|string',
            'email'=>'bail|required|email|unique:admins',
            'phoneNumber'=>'bail|required|max:11|min:11|unique:admins,phoneNumber|starts_with:09',
            'password'=>'bail|required|min:6|confirmed',
            'avatar'=>'bail|required',
            'avatar.*'=> 'mimes:jpg,png,jpeg',
        ];
    }
}
