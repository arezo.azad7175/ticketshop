<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class addTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'company'=>'bail|required|string',
            'origin'=>'bail|required|string',
            'destination'=>'bail|required|string|different:origin',
            'price'=>'bail|required|integer|min:100000',
            'date'=>'bail|required',
            'time'=>'bail|required',
            'capacity'=>'bail|required|integer|min:1',
        ];
    }
}
