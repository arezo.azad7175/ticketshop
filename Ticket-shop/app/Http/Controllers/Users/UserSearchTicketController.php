<?php

namespace App\Http\Controllers\Users;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\SearcchTicketRequest;

class UserSearchTicketController extends Controller
{
    public function searchTicket(Request $request , SearcchTicketRequest $SearcchTicketRequest)
    {
        // check exist ticket
        $ticketExist = Order::whereTracking_code($request->code)->exists();

        // if found & user paied successful show that
        if($ticketExist)
        {
            $order = Order::whereTracking_code($request->code)->first();

            if($order->payment == true)
            {
                return view('Users.resultSearchTicket' , ['order' => $order]);
            }
           
        }

        return redirect()->back()->with('notFountTicket' , 'بلیط درخواستی شما وجود ندارد');
    }
}
