<?php

namespace App\Http\Controllers\Users;

use App\Models\User;
use App\Models\Order;
use App\Helper\Helper;
use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class PaymentController extends Controller
{
    public function create()
    {
   
        $order = Order::whereId(Session::get('order_id'))->first();
        $url = "https://panel.aqayepardakht.ir/api/v2/create";

        $data = 
        [
            'invoice_id'=> $order->id,
            'pin'=> 'sandbox',
            'amount'=> (int)$order->amount,
            'callback'=>route('payment.verify'),     
        ];

        $response = Http::withOptions(['verify' => false]) ->post($url , $data);

        if($response->status() === 200)
        {
            $transId = $response->json('transid');

            return redirect()->away("https://panel.aqayepardakht.ir/startpay/sandbox/$transId");
        }

        return redirect()->back()->with('payment' , 'امکان اتصال به درگاه بانکی وجود ندارد');
    }


    public function verify(Request $request)
    {
        // order
        $order = Order::find($request->get('invoice_id'));

        // user_Auth
        $user = $order->user;
        Auth::guard('web')->login($user);

        if($request->has('status') && $request->get('status') === '1')
        {

            $data = 
            [
                'pin' => 'sandbox',
                'amount' =>$order->amount,
                'transid' => $request->get('transid'),
            ];

            $url =  'https://panel.aqayepardakht.ir/api/v2/verify';

            $response = Http::withOptions(['verify' => false])->post($url , $data);

            // success_pay
            if($response->status() === 200 && $response->json('status') === 'success')
            {
                // edit order field for user
                $order->payment = true;
                $order->tracking_code = $request->get('transid');
                $order->save();

                // reduce count of ticket table
                $order->ticket->capacity -= $order->count;
                $order->ticket->save();

                // send sms
                $mobile = $user->phoneNumber;
                Helper::sendSuccessPaymentSms($mobile , $request->get('transid'));

                return redirect()->route('buyTicket')->with('payment' , 'بلیط شما با موفقیت خریداری و رزرو شد');

            }

            return redirect()->route('buyTicket')->with('payment' , 'پرداخت بنا به دلایلی انجام نشد شاید قبلا تایید شده باشد');    
        
        }
        // delete order of DB
        $order->delete();
        
        return redirect()->route('buyTicket')->with('payment' , 'درخواست غیرمجاز'); 
    }
}
