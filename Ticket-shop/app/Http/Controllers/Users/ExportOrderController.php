<?php

namespace App\Http\Controllers\Users;

use App\Models\Order;
use Illuminate\Http\Request;
use PDF;
use App\Http\Controllers\Controller;

class ExportOrderController extends Controller
{
    public function exportOrder($order_id)
    {
        // get order
        $order = Order::whereId($order_id)->first();
        // get pdf of order
        $pdf = PDF::loadView('Users.resultSearch_pdf' , ['order' => $order]);
        // download pdf
        return $pdf->download('ticket.pdf');
    }
}
