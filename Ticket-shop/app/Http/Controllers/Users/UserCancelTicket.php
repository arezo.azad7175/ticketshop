<?php

namespace App\Http\Controllers\Users;

use Cryptommer\Smsir\Smsir;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserCancelTicket extends Controller
{
    public function cancelTicket($order_id)
    {
        $user = Auth::guard('web')->user();
        $order = Order::whereId($order_id)->first();

        // edit record in DB
        $order->tracking_code = "cancel";
        $order->payment = false;
        $order->save();
        
        // return value
        $returnAmount = ($order->amount - 10000);

        // send sms
        $message = "بلیط موردنظر شما با کسر 10 هزار تومان از مبلغ کل لغو گردید"."\n"."شما مبلغ $returnAmount را طی 24 ساعت آینده دریافت خواهید نمود."."\n"."\n"."سفیر";
        $send = smsir::Send();
        $send->bulk($message, [(string)$user->phoneNumber], null, null);

        return redirect()->route('searchTicket')->with('cancelTicket' , 'بلیط شما با موفقیت لغو گردید و طی 24 ساعت مبلغ عودت داده خواهد شد');
    }
}
