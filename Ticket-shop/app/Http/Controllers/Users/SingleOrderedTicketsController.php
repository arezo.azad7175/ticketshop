<?php

namespace App\Http\Controllers\Users;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SingleOrderedTicketsController extends Controller
{
    public function showOrderedTickets($order_id)
    {
        $order = Order::where('id' , $order_id)->first();
        return view('Users.resultSearchTicket' , ['order' => $order]);
    }
}
