<?php

namespace App\Http\Controllers\Users;

use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\BuyTicketRequest;

class BuyTicketSearchController extends Controller
{
    public function buyTicketSearch(Request $request , BuyTicketRequest $BuyTicketRequest)
    {
        // get ticket with user data
        $tickets = Ticket::where([
            ['origin' ,$request->origin],
            ['destination' , $request->destination ],
            ['date' , $request->dateGo],
        ])->orWhere([
            ['origin' ,$request->destination],
            ['destination' , $request->origin],
            ['date' , $request->dateBack],
        ])->get();

        // if not found
        if($tickets->isEmpty())
        {
            return redirect()->back()->with('notFountTicket' , 'بلیط مورد نظر شما پیدا نشد');
        }
        return view('Users.buyTicket' , ['tickets' => $tickets]);

    }
}
