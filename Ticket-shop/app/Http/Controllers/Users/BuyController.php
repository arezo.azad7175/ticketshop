<?php

namespace App\Http\Controllers\Users;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BuyController extends Controller
{
    public function buy(Request $request , $id )
    {
        //get date
        $carbon = new Carbon;
        
        // find ticket
        $ticket = Ticket::whereId($id)->first();
        // get overal amount
        $amount = ($ticket->price) * ($request->count);
        // get user
        $user = Auth::guard('web')->user();
        // set order in DB
        $order = Order::create([
            'user_id' => $user->id,
            'ticket_id' => $id ,
            'count' => $request->count,
            'amount' => $amount,
            'tracking_code'=> null,
            'payment'=>false,
            'created_at'=>Carbon::now()->format('Y-m-d'),
            'updated_at'=>Carbon::now()->format('Y-m-d'),
        ]);

        return redirect()->route('payment.create')->with('order_id' , $order->id);
    
    }
}
