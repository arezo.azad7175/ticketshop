<?php

namespace App\Http\Controllers\Users\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserLogoutController extends Controller
{
    public function userlogout()
    {  
        Auth::guard('web')->logout();
        return redirect('/safir');
    }
}
