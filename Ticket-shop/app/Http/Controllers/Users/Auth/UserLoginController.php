<?php

namespace App\Http\Controllers\Users\Auth;

use App\Models\User;
use App\Helper\Helper;
use Cryptommer\Smsir\Smsir;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Users\UserPassRequest;
use App\Http\Requests\Users\UserRegisterRequest;

class UserLoginController extends Controller
{

    public function userLogin(Request $request , UserRegisterRequest $UserRegisterRequest)
    {
        $userExist = User::where('nationalCode',$request->nationalCode)->doesntExist();

        if(!$userExist)
        {
            if(User::where('nationalCode' , $request->nationalCode)->where('phoneNumber' , $request->phoneNumber))
            {
                // refresh sessions
                $request->session()->regenerate();

                // user
                $user = User::where('nationalCode',$request->nationalCode)->first();

                // send password with sms
                $mobile = $user->phoneNumber;
                $verifyCode = Helper::sendVerifyCodeSms($mobile);

                // update_pass_in_DB
                $user->update(['password' => $verifyCode]);

                return redirect()->route('user.pass')->with('mobile' , $mobile);

            }
        }

        else
        {
            // send password with sms
            $mobile = $request->phoneNumber;
            $verifyCode = Helper::sendVerifyCodeSms($mobile);
            
            // register
            $user = User::create([
                'name' => $request->name,
                'nationalCode'=>$request->nationalCode,
                'phoneNumber' => $request->phoneNumber,
                'password'=> $verifyCode,
            ]);

             
            return redirect()->route('user.pass')->with('mobile' , $mobile);

        }
        
    }


    public function checkUserPass(Request $request , UserPassRequest $UserPassRequest , $mobile)
    {
        // user
        $user =  User::where('phoneNumber',$mobile)->first();

        // check_password
        if(Hash::check($request->passwordUser , $user->password))
        {
            // login
            Auth::guard('web')->login($user);

            return redirect()->route('buyTicket');
        }
        else
        {
            return redirect()->back()->with('wrongPass','رمز عبور وارد شده نادرست است');
        }
    }
}
