<?php

namespace App\Http\Controllers\Users;

use App\Models\User;
use App\Models\Order;
use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserPagesController extends Controller
{
    public function userLoginShow()
    {
        return view('Users.Auth.loginUser');
    }

    public function userPassShow()
    {
        return view('Users.Auth.passUser');
    }

    public function buyTicketShow()
    {
        // get all ticket
        $tickets = Ticket::all();
        return view('Users.buyTicket' , ['tickets' => $tickets]);
    }

    public function showSearchTicket()
    {
        return view('Users.searchTicket');
    }

    public function showOrderedTickets()
    {
        // get logined user
        $user =Auth::guard('web')->user();
        // get users orders
        $orders = Order::where('user_id' , $user->id)->where('payment',true)->orderBy('created_at', 'DESC')->get();
        
        return view('Users.OrderedTickets' , ['orders' => $orders]);
    }

}
