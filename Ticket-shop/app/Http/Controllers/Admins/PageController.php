<?php

namespace App\Http\Controllers\Admins;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function adminRegisterShow()
    {
        return view('Admins.Auth.registerAdmin');
    }

    public function adminLoginShow()
    {
        return view('Admins.Auth.loginAdmin');
    }

    public function adminForgetPassShow()
    {
        return view('Admins.Auth.forgetPass');
    }

    public function showDashboard()
    {   
        return view('Admins.dashboard'); 
    }


    public function showAddTicket()
    {
        return view('Admins.addTicket');
    }

    public function showWelcom()
    {
        return view('Admins.welcom');
    }

    public function showUsersInfo()
    {
        $users = User::all();
        return view('Admins.showUsersInfo' , ['users' => $users]);
    }
}
