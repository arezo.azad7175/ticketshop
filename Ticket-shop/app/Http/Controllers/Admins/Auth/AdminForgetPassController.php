<?php

namespace App\Http\Controllers\Admins\Auth;

use App\Models\Admin;
use App\Mail\ForgetPassMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AdminForgetPassController extends Controller
{
    public function setNewPass(Request $request)
    {
        $admin = Admin::whereEmail($request->email)->first();

        if($admin)
        {
            // send email with new password
            $password = rand(10000000,99999999);
            Mail::to($admin->email)->send( New ForgetPassMail($password));

            // save password to DB
            $data = ['password'=>Hash::make($password)];
            $admin->update($data);

            // back to login
            return redirect(route('admin.login'))->with('passSend' , 'ایمیل حاوی پسورد جدید ارسال شد');

        }
        return redirect()->back()->with('passError' , 'کاربری با این ایمیل ثبت نام نکرده است' );
    }
}
