<?php

namespace App\Http\Controllers\Admins\Auth;

use App\Models\Admin;
use App\Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Admins\RegisterRequest;


class AdminRegisterController extends Controller
{
    public function adminRegister(Request $request , RegisterRequest  $registerRequest )
    {
        // create Admin in DB
        $admin = Admin::create([
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'email' => $request->email,
            'phoneNumber' => $request->phoneNumber,
            'password'=>Hash::make($request->password),
            'avatar' =>$request->avatar,   
        ]);
 
        // set avatar
        $avatar = $request->file('avatar');
        Helper::setAvatar($admin , $avatar);

  
        // login and show dashbord
        Auth::guard('admin')->login($admin);

        return redirect()->route('welcom');

    }
}
