<?php

namespace App\Http\Controllers\Admins\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminLogoutController extends Controller
{
    public function adminlogout()
    {  
        Auth::guard('admin')->logout();
        return redirect('/');
    }
}
