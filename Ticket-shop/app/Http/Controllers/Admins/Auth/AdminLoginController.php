<?php

namespace App\Http\Controllers\Admins\Auth;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    public function adminLogin(Request $request) 
    {
        // check email & password
        if(Auth::guard('admin')->attempt(['email' => $request->email , 'password' => $request->password]))
        {
            // set session
            $request->session()->regenerate();

            // login
            $admin = Admin::whereEmail($request->email)->first();
            Auth::guard('admin')->login($admin);
            
            return redirect()->route('welcom');
        }

        return redirect()->back()->with('loginError' , 'نام کاربری یا رمز عبور اشتباه است');
            
    }
}
