<?php

namespace App\Http\Controllers\Admins;

use App\Models\Order;
use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\SearchTicketRequest;


class ShowTicketsController extends Controller
{
    public function showTickets()
    {
        // get tickets
        $tickets = Ticket::all();
        return view('Admins.showTickets' , ['tickets' => $tickets]);
    }


    public function destroyTicket($id)
    {
        // find ticket id & delete
        Ticket::whereId($id)->delete();
        
        if(Order::whereTicket_id($id)->exists())
        {
            Order::whereTicket_id($id)->delete();
        }

        return redirect()->back()->with('destroyTicket', 'بلیط مورد نظر با موفقیت حذف شد');
    }


    public function searchTickets(Request $request ,SearchTicketRequest $SearchTicketRequest)
    {
        // find ticket
        $tickets = Ticket::whereOrigin($request->origin)->whereDestination($request->destination)->get();

        // if not found
        if($tickets->isEmpty())
        {
            return redirect()->back()->with('notFountTicket' , 'بلیط مورد نظر شما پیدا نشد');
        }
        return view('Admins.showTickets' , ['tickets' => $tickets]);

    }
}
