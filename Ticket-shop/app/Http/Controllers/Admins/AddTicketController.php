<?php

namespace App\Http\Controllers\Admins;

use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\Admins\addTicketRequest;

class AddTicketController extends Controller
{
    public function addTicket( Request $request , addTicketRequest $addTicketRequest )
    {
        // add ticket
        Ticket::create([
            'company'=>$request->company,
            'origin'=>$request->origin,
            'destination'=>$request->destination,
            'price'=>$request->price,
            'date'=>$request->date,
            'time'=>$request->time,
            'capacity'=>$request->capacity,
        ]);

        // set session
        Session::flash('success','بلیط شما با موفقیت ثبت شد');
        return redirect()->back();
    }//
}
