<?php

namespace App\Http\Controllers\Admins;

use App\Models\User;
use App\Models\Order;
use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function dashboardValue()
    {
        // create first of month date till now
        $carbon = new Carbon;
        $start = Carbon::now()->firstOfMonth()->format('Y-m-d');
        $end = Carbon::now();

        // get data of dataBase
        $user =User::count();
        $order = Order::whereBetween('created_at' ,  [$start , $end ] )->count();
        $ticket = Ticket::count();

        return redirect()->route('dashboard')->with(['users' =>$user , 'orders' => $order , 'tickets' => $ticket] );

    }

}
