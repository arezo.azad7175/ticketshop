<?php
namespace App\Helper;


use Carbon\Carbon;
use Cryptommer\Smsir\Smsir;
use Illuminate\Support\Facades\Storage;

class Helper
{
    // set avatar
    public static function setAvatar($admin , $avatar)
    {
        if($admin->avatar)
        {
            Storage::delete($admin->avatar);
        }

        // update image in DB and empty that column
        $data =['avatar'=>null];
        $admin->update($data);
        
        
        // save avatar
        $fileName = Storage::put('/avatars' , $avatar);
        
        
        // save file name in DB
        $data = ['avatar'=>$fileName];
        $admin->update($data);
        

        // get avatar of DB
        $avatarFileName = $admin->avatar;

        // send avatar name
        return(Storage::get($avatarFileName));
              
    }


    public static function sendVerifyCodeSms($mobile)
    {
        $password = rand(10000000,99999999);
        $parameter = new \Cryptommer\Smsir\Objects\Parameters("CODE",(string)$password);
        $parameters = array($parameter);
 
        $send = smsir::Send();
        $send->Verify((string)$mobile, 614037, $parameters);

        return $password;
    }

    public static function sendSuccessPaymentSms($mobile , $code)
    {
        $parameter = new \Cryptommer\Smsir\Objects\Parameters("CODE",(string)$code);
        $parameters = array($parameter);

        $send = smsir::Send();
        $send->Verify((string)$mobile, 134259, $parameters);

    }



}