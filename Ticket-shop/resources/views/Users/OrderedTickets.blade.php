@extends('Users.layout.master')


@section('content')

<!-- show ticket table -->
<div class="row tbl-orderd">
    <div class="col-12">

     <table>
     <tbody>
        <tr>
            <th id="null-th"></th>
            <th>قیمت نهایی</th>
            <th>تعداد</th>
            <th>ساعت</th>
            <th>تاریخ</th>
            <th>کدپیگیری</th>
            <th>مقصد</th>
            <th>مبدا</th> 
            <th>تاریخ خرید</th> 
            <th>ردیف</th> 
        </tr>
       
       @php
          $i=0;
       @endphp

       @foreach($orders as $order)
          <tr>
              @php
                $i++;
              @endphp
              <td><button><a href="{{route('SingleOrderedTickets' , $order->id)}}">نمایش</a></button></td>
              <td>{{$order->amount}}</td>
              <td>{{$order->count}}</td>
              <td>{{$order->ticket->time}}</td>
              <td>{{$order->ticket->date}}</td>
              <td>{{$order->tracking_code}}</td>
              <td>{{$order->ticket->destination}}</td>
              <td>{{$order->ticket->origin}}</td>
              <td>{{$order->created_at}}</td>
              <td>{{ $i }}</td>
           <tr>
        
       @endforeach
     </tbody>

     </table>
    </div>
</div>
@endsection