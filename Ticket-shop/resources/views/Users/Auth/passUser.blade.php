<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- title & icon -->
    <title>Safir</title>
    <link rel="shortcut icon" href="{{asset('img/bus_icon.jpg')}}" type="image/x-icon">
    <!-- bootstrap links -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    <!-- css -->
    @vite(['resources/css/Users/userAuth.css'])

</head>
<body>

<!-- wrong password alert -->
@if(Session::has('wrongPass'))

<div class="alert alert-warning alert-dismissible fade show" role="alert">
     <strong>نادرست !</strong> {{ Session::get('wrongPass') }}
     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>

@endif

    <div class="container-fluid">
        <div class="row">
            <div class="m-2 col-12 col-lg-6 shadaw user">
                    <h3>ورود رمز </h3>
                    <br>
                    <!-- check user password with validation-->
                    <form action="{{route('userPass.post' ,(string)Session::get('mobile'))}}" method="post" >
                        @csrf
                        <!-- password -->
                        <div class="mb-6 col-md-12">
                            <label for="passwordUser">لطفا <strong style="color:#e9a100">کد تاییدیه </strong> پیامک شده به شماره <h5 style="color:#e9a100"> {{Str::mask(Session::get('mobile') ,'*' , 6 )}} </h5> را وارد کنید</label>
                            <input type="password" name = 'passwordUser' class="form-control  @error('passwordUser') is-invalid @enderror">
                            @error('passwordUser')
                               <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>


                        <div class="mb-3 col-12">
                            <div class=" mt-50">
                                <button type="submit" class=" form-control btn btn-success"> ورود رمز</button>
                            </div>
                        </div>
            
                    </form>
            </div>
        </div>
    </div>

</body>
</html>