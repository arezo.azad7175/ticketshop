@extends('Users.layout.master')

@section('content')

<!-- not found ticket alert -->
@if(Session::has('notFountTicket'))
   <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>ناموفق !</strong> {{ Session::get('notFountTicket') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
   </div>
@endif

<!-- payment alert -->
@if(Session::has('payment'))
   <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>پرداخت !</strong> {{ Session::get('payment') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
   </div>
@endif

<!-- search ticket -->
<a href="{{route('buyTicket')}}"><img src="{{asset('img/first.svg')}}" class="back"></a>
<div class="row search-box">
    <div class="col-12"> 
        <!-- buy ticket search form with validation-->
        <form action="{{route('buyTicketSearch')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-6 col-md-12">
              <!-- origin -->
              <label for="origin">مبدا</label>
              <select name="origin" id="origin" class="form-control @error('origin') is-invalid @enderror" value="{{old('origin')}}">
                  <option value="تهران">تهران</option>
                  <option value="زنجان">زنجان</option>
                  <option value="اصفهان">اصفهان</option>
                  <option value="مشهد">مشهد</option>
                  <option value="تبریز">تبریز</option>
                  <option value="اردبیل">اردبیل</option>
                  <option value="قم">قم</option>
              </select>
              @error('origin')
                   <p class="text-danger">{{$message}}</p>
              @enderror
            </div>

            <!-- destination -->
            <div class="mb-6 col-md-12">
              <label for="destination">مقصد</label>
              <select name="destination" id="destination" class="form-control @error('destination') is-invalid @enderror" value="{{old('destination')}}">
                 <option value="تهران">تهران</option>
                 <option value="زنجان">زنجان</option>
                 <option value="اصفهان">اصفهان</option>
                 <option value="مشهد">مشهد</option>
                 <option value="تبریز">تبریز</option>
                 <option value="اردبیل">اردبیل</option>
                 <option value="قم">قم</option>
              </select>
              @error('destination')
                   <p class="text-danger">{{$message}}</p>
              @enderror
            </div>

            <!-- go date -->
            <div class="mb-6 col-md-12">
               <label for="dateGo"> تاریخ رفت</label>
               <input type="date" name="dateGo" id="dateGo" class="form-control @error('dateGo') is-invalid @enderror" value="{{old('dateGo')}}">
               @error('dateGo')
                   <p class="text-danger">{{$message}}</p>
               @enderror
            </div>

            <!-- return date -->
            <div class="mb-6 col-md-12">
               <label for="dateBack"> تاریخ برگشت</label>
               <input type="date" name="dateBack" id="dateBack" class="form-control @error('dateBack') is-invalid @enderror" value="{{old('dateBack')}}">
               @error('dateBack')
                   <p class="text-danger">{{$message}}</p>
               @enderror
            </div>

            <br>

            <div class="mb-3 col-12">
                <div class=" mt-50">
                    <button type="submit" class=" form-control btn btn-success">جستجو</button>
                </div>
            </div>
        </form>

    </div>
</div>


<!-- show ticket table -->
<div class="row tbl-tickets">
    <div class="col-12">

     <table>
     <tbody>
        <tr>
          <th id="null-th"></th>
          <th>قیمت</th>
          <th>ظرفیت</th>
          <th>ساعت</th>
          <th>تاریخ</th>
          <th>شرکت</th>
          <th>مقصد</th>
          <th>مبدا</th>  
          <th>ردیف</th> 
       </tr>
       
       @php
          $i=0;
       @endphp
       <!-- show tickets -->
       @foreach($tickets as $ticket)
          <tr>
              @php
                $i++;
              @endphp
              <td>
                <!-- buy form -->
                <form action="{{route('buy' , $ticket->id)}}" method="post">
                  @csrf
                  <button class ="buy-btn">خرید</button>
                  <input type="number" class="count" min= 1 name = "count" value = 1>
                </form>
     
              </td>
              <td>{{$ticket->price}}</td>
              <td>{{$ticket->capacity}}</td>
              <td>{{$ticket->time}}</td>
              <td>{{$ticket->date}}</td>
              <td>{{$ticket->company}}</td>
              <td>{{$ticket->destination}}</td>
              <td>{{$ticket->origin}}</td>
              <td>{{ $i }}</td>
           <tr>
        
       @endforeach
     </tbody>

     </table>
    </div>
</div>


@endsection