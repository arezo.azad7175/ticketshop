<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- titile $ icon -->
    <title>Safir</title>
    <link rel="shortcut icon" href="{{asset('img/bus_icon.jpg')}}" type="image/x-icon">

    <!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
 
    <!-- css -->
    @vite(['resources/css/Users/user.css'])

</head>
<body >

  <div class="container-fluid">

       <div class="row header">

          <div class="col-12">
            <!-- get logined user -->
              @php
                 $user = Auth::guard('web')->user();
              @endphp
              <!-- home -->
              <a href="{{route('buyTicket')}}"><img src="{{asset('img/home.svg')}}" class="home"></a>
              
              <!-- name of user -->
              <p class="userName">{{$user->name}}</p>
              
              <!-- search -->
              <button class="btn-search"><a href="{{route('searchTicket')}}">جستجو با کد پیگیری</a></button>
              
              <!-- bought tickets -->
              <button class="btn-tickets"><a href="{{route('orderedTickets')}}">بلیط های خریداری شده</a></button>
              
              <!-- logout -->
              <form action="{{route('userlogout')}}" method="post"  class="logout">
                @csrf
                <input type="submit" value="خروج">
              </form>
          </div>

      </div>
      
  </div>

  <!-- content -->
  @yield('content')
  
   
<!-- footer -->
<div class="footer">

Lorem ipsum dolor sit amet, consectetur adipiscing elit,
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
 Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
<br>nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehender
</div>

<!-- script links -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
</body>
</html>