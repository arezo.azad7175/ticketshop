@extends('Users.layout.master')

@section('content')

    <div class = "row showSearchedTicket">
        <!-- image -->
        <div class="col-3">
            <img src="{{asset('img/buspng.png')}}">
        </div>
        <!-- ticket info -->
        <div class="col-3">
            <p class="title">کدپیگیری:</p><p>{{$order->tracking_code}}</p>
            <p class="title">نام مسافر:</p><p>{{$order->user->name}}</p>
            <p class="title">بهای هر صندلی:</p><p>{{$order->ticket->price}}</p>
        </div>

        <div class="col-3">
            <p class="title">تاریخ حرکت:</p><p>{{$order->ticket->date}}</p>
            <p class="title">مبدا:</p>{{$order->ticket->origin}}<p></p>
            <p class="title">تعداد صندلی:</p><p>{{$order->count}}</p>
        </div>

        <div class="col-3">
            <p class="title">ساعت حرکت:</p><p>{{$order->ticket->time}}</p>
            <p class="title">مقصد:</p>{{$order->ticket->destination}}<p></p>
            <p class="title">بهای کل:</p><p>{{$order->amount}}</p>
        </div>

        <div class="row">
            <div class="col-12">
                <p class="cancel_Ticket"><a href=" {{route('cancelTicket' , $order->id)}}">لغو بلیط .</a></p>
                <p class="resive_pdf"><a href="{{route('exportOrder' , $order->id)}}">دریافت بلیط .</a></p>

                <!-- footer -->
                <p class="description">در صورت بروز مشکل با شماره 09121234567 تماس بگیرید</p>
    
            </div>
        </div>

    </div>
@endsection