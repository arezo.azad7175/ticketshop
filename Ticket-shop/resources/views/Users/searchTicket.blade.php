@extends('Users.layout.master')

@section('content')

<!-- not found ticket alert -->
@if(Session::has('notFountTicket'))
   <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>ناموفق !</strong> {{ Session::get('notFountTicket') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
   </div>
@endif

<!-- cancel ticket alert -->
@if(Session::has('cancelTicket'))
   <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>موفق !</strong> {{ Session::get('cancelTicket') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
   </div>
@endif

<div class="row  code-box">
    <div class="col-12">
        <!-- search order ticket form with validation-->
        <form action="{{route('search.OrderTicket')}}" method="post">
         @csrf
            <!-- code for search -->
            <div class="mb-6 col-md-12" >
                <input type="text" name = 'code' class="form-control  @error('code') is-invalid @enderror" placeholder="کد پیگیری خود را وارد کنید" value="{{old('code')}}">
                @error('code')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>  
            <br>
            <br>
            <div class="mb-6 col-md-12">
                <input type="submit" value="جستجو" class="form-control search-btn">
            </div>
        </form>
    </div>
</div>

</div>

@endsection