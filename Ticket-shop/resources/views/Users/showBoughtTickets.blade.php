@extends('Users.layout.master')

@section('content')


<!-- show ticket table -->
<div class="row tbl-tickets">
    <div class="col-12">
     <table>
     <tbody>
        <tr>
          <th id="null-th"></th>
          <th>قیمت</th>
          <th>ظرفیت</th>
          <th>ساعت</th>
          <th>تاریخ</th>
          <th>شرکت</th>
          <th>مقصد</th>
          <th>مبدا</th>  
          <th>ردیف</th> 
       </tr>
       
       @php
          $i=0;
       @endphp

       <!-- show tickets -->
       @foreach($tickets as $ticket)
          <tr>
              @php
                $i++;
              @endphp
              <td><button><a href="{{route('destroyTicket' , $ticket->id)}}">نمایش</a></button></td>
              <td>{{$ticket->price}}</td>
              <td>{{$ticket->capacity}}</td>
              <td>{{$ticket->time}}</td>
              <td>{{$ticket->date}}</td>
              <td>{{$ticket->company}}</td>
              <td>{{$ticket->destination}}</td>
              <td>{{$ticket->origin}}</td>
              <td>{{ $i }}</td>
           <tr>
        
       @endforeach
     </tbody>

     </table>
    </div>
</div>

@endsection