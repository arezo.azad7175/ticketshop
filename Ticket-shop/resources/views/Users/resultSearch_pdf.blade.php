<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- titile $ icon -->
    <title>Safir</title>
    <link rel="shortcut icon" href="{{asset('img/bus_icon.jpg')}}" type="image/x-icon">

    <!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
 
    <!-- css -->
    @vite(['resources/css/Users/user.css'])

</head>
<body >
<div class = "row showSearchedTicket">
        <!-- image -->
        <div class="col-3">
            <img src="{{asset('img/buspng.png')}}">
        </div>
        <!-- info of ticket -->
        <div class="col-3">
            <p class="title">کدپیگیری:</p><p>{{$order->tracking_code}}</p>
            <p class="title">نام مسافر:</p><p>{{$order->user->name}}</p>
            <p class="title">بهای هر صندلی:</p><p>{{$order->ticket->price}}</p>
        </div>

        <div class="col-3">
            <p class="title">تاریخ حرکت:</p><p>{{$order->ticket->date}}</p>
            <p class="title">مبدا:</p>{{$order->ticket->origin}}<p></p>
            <p class="title">تعداد صندلی:</p><p>{{$order->count}}</p>
        </div>

        <div class="col-3">
            <p class="title">ساعت حرکت:</p><p>{{$order->ticket->time}}</p>
            <p class="title">مقصد:</p>{{$order->ticket->destination}}<p></p>
            <p class="title">بهای کل:</p><p>{{$order->amount}}</p>
        </div>

        <!-- footer -->
        <div class="row">
            <div class="col-12">
                <p class="description">در صورت بروز مشکل با شماره 09121234567 تماس بگیرید</p>
            </div>
        </div>
</div>

</body>