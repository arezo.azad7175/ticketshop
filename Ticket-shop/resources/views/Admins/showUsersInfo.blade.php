@extends('Admins.layout.master')
@section('content')

<div class="row tbl-users">
    <div class="col-12">
      <!-- table of users -->
     <table>
        <tbody>
          <tr>
            <th>شماره تماس</th>
            <th>کدملی</th>
            <th>نام</th>
            <th>تاریخ ثبت نام </th>
          </tr>
        
         @php
            $i=0;
         @endphp

         @foreach($users as $user)
            <tr>
                @php
                    $i++;
                @endphp
                <td>{{$user->phoneNumber}}</td>
                <td>{{$user->nationalCode}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->created_at}}</td>
                <td>{{ $i }}</td>
            <tr>
        
         @endforeach
       </tbody>
     </table>
  </div>
</div>
@endsection