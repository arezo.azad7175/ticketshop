@extends('Admins.layout.master')

@section('content')

<!-- deleted ticket alert -->
@if(Session::has('destroyTicket'))

   <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>موفق !</strong> {{ Session::get('destroyTicket') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
   </div>

@endif

<!-- not fount ticket alert -->
@if(Session::has('notFountTicket'))

   <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>ناموفق !</strong> {{ Session::get('notFountTicket') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
   </div>

@endif


<!-- search ticket -->
<div class="row search-box">
    <div class="col-12">
      <!--search ticket form with validation -->
        <form action="{{route('searchTickets')}}" method="post">
            @csrf

            <!-- origin -->
            <div class="mb-6 col-md-12">
              <label for="origin">مبدا</label>
              <select name="origin" id="origin" class="form-control @error('origin') is-invalid @enderror" value="{{old('origin')}}">
                  <option value="تهران">تهران</option>
                  <option value="زنجان">زنجان</option>
                  <option value="اصفهان">اصفهان</option>
                  <option value="مشهد">مشهد</option>
                  <option value="تبریز">تبریز</option>
                  <option value="اردبیل">اردبیل</option>
                  <option value="قم">قم</option>
              </select>
              @error('origin')
                   <p class="text-danger">{{$message}}</p>
              @enderror
            </div>

            <!-- destination -->
            <div class="mb-6 col-md-12">
              <label for="destination">مقصد</label>
              <select name="destination" id="destination" class="form-control @error('destination') is-invalid @enderror" value="{{old('destination')}}">
                 <option value="تهران">تهران</option>
                 <option value="زنجان">زنجان</option>
                 <option value="اصفهان">اصفهان</option>
                 <option value="مشهد">مشهد</option>
                 <option value="تبریز">تبریز</option>
                 <option value="اردبیل">اردبیل</option>
                 <option value="قم">قم</option>
              </select>
              @error('destination')
                   <p class="text-danger">{{$message}}</p>
              @enderror
            </div>

            <br>

            <div class="mb-3 col-12">
                <div class=" mt-50">
                    <button type="submit" class=" form-control btn btn-success">جستجو</button>
                </div>
            </div>
        </form>

    </div>
</div>



<!-- show ticket table -->
<div class="row tbl-tickets">
    <div class="col-12">

     <table>
     <tbody>
        <tr>
          <th id="null-th"></th>
          <th>قیمت</th>
          <th>ظرفیت</th>
          <th>ساعت</th>
          <th>تاریخ</th>
          <th>شرکت</th>
          <th>مقصد</th>
          <th>مبدا</th>  
          <th>ردیف</th> 
       </tr>
       
       @php
          $i=0;
       @endphp

       @foreach($tickets as $ticket)
          <tr>
              @php
                $i++;
              @endphp
              <td><button><a href="{{route('destroyTicket' , $ticket->id)}}">حذف</a></button></td>
              <td>{{$ticket->price}}</td>
              <td>{{$ticket->capacity}}</td>
              <td>{{$ticket->time}}</td>
              <td>{{$ticket->date}}</td>
              <td>{{$ticket->company}}</td>
              <td>{{$ticket->destination}}</td>
              <td>{{$ticket->origin}}</td>
              <td>{{ $i }}</td>
           <tr>
        
       @endforeach
     </tbody>

     </table>
    </div>
</div>


@endsection