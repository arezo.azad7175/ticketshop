<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- title & icon -->
    <title>Safir</title>
    <link rel="shortcut icon" href="{{asset('img/bus_icon.jpg')}}" type="image/x-icon">
    <!-- bootstrap links -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    <!-- css -->
    @vite(['resources/css/Admins/admin.css'])

</head>
<body >

  <div class="container-fluid">
       <div class="row row-1">
          <div class="col-12">
            <!--get logined user info-->
              @php
              $user = Auth::guard('admin')->user();
              @endphp
              <!-- image -->
              <img src='{{Storage::url($user->avatar)}}' class="avt">

              <!-- name of user -->
              <p class="name">{{$user->firstName}}</p>

              <!-- logout user button -->
              <form action="{{route('adminlogout')}}" method="post"  class="logout">
                @csrf
                <input type="submit" value="خروج">
              </form>
          </div>
      </div>

      <div class="row row-2">
        <div class="col-12">
          <!-- users -->
           <p class="users"><a href="{{route('users.info')}}">کاربران</a>
           <!-- tickets -->
           <div class="ticket-menu">
              <p class="dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                بلیط ها
              </p>
              <ul class="dropdown-menu">
                <!-- show tickets -->
                  <li><a class="dropdown-item" href="{{route('showTickets')}}">نمایش بلیط ها</a></li>
                  <!-- add tickets -->
                  <li><a class="dropdown-item" href="{{route('addTicket')}}">اضافه کردن بلیط</a></li>
              </ul> 
            </div>

            <!-- dashboard -->
            <p class="dashboard"><a href="{{route('dashboardValue')}}">داشبورد</a></p>
        </div>
 
      </div>
      
  </div>


  <!-- content -->
  @yield('content')
  

<!-- footer -->
<div class="footer">

Lorem ipsum dolor sit amet, consectetur adipiscing elit,
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
 Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
<br>nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehender
</div>

<!-- script links -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>

</body>
</html>