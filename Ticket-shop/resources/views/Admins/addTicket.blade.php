@extends('Admins.layout.master')
@section('content')

<!-- add ticket successfull alert -->
@if(Session::has('success'))
   <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>موفق !</strong> {{ Session::get('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
   </div>

@endif


<div class="row">
    <div class="col-12">
        <!-- add ticket form with validation-->
        <form action="{{route('addTicket.post')}}" method="post" class="add">
            @csrf

            <div class="mb-6 col-md-12">
                <!-- companies -->
              <label for="company">شرکت</label>
              <select name="company" id="company" class="form-control @error('company') is-invalid @enderror" value="{{old('company')}}">
                  <option value="ایران پیما">ایران پیما</option>
                  <option value="سیر و سفر">سیر و سفر</option>
                  <option value="میهن نور">میهن نور</option>
                  <option value="لوان نور">لوان نور</option>
                  <option value="ایمن سفر">ایمن سفر</option>
                  <option value="پیک معتمد">پیک معتمد</option>
                  <option value="کیان سفر">کیان سفر</option>
              </select>
              @error('company')
                   <p class="text-danger">{{$message}}</p>
              @enderror
            </div>

            <!-- origin -->
            <div class="mb-6 col-md-12">
              <label for="origin">مبدا</label>
              <select name="origin" id="origin" class="form-control @error('origin') is-invalid @enderror" value="{{old('origin')}}">
                  <option value="تهران">تهران</option>
                  <option value="زنجان">زنجان</option>
                  <option value="اصفهان">اصفهان</option>
                  <option value="مشهد">مشهد</option>
                  <option value="تبریز">تبریز</option>
                  <option value="اردبیل">اردبیل</option>
                  <option value="قم">قم</option>
              </select>
              @error('origin')
                   <p class="text-danger">{{$message}}</p>
              @enderror
            </div>

            <!-- destination -->
            <div class="mb-6 col-md-12">
              <label for="destination">مقصد</label>
              <select name="destination" id="destination" class="form-control @error('destination') is-invalid @enderror" value="{{old('destination')}}">
                 <option value="تهران">تهران</option>
                 <option value="زنجان">زنجان</option>
                 <option value="اصفهان">اصفهان</option>
                 <option value="مشهد">مشهد</option>
                 <option value="تبریز">تبریز</option>
                 <option value="اردبیل">اردبیل</option>
                 <option value="قم">قم</option>
              </select>
              @error('destination')
                   <p class="text-danger">{{$message}}</p>
              @enderror
            </div>

            <!-- price -->
            <div class="mb-6 col-md-12">
                <label for="price">قیمت</label>
                <input type="number" name="price" id="price" class="form-control @error('price') is-invalid @enderror" value="{{old('price')}}" placeholder="100,000">
                @error('price')
                   <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <!-- date -->
            <div class="mb-6 col-md-12">
               <label for="date"> تاریخ حرکت</label>
               <input type="date" name="date" id="date" class="form-control @error('date') is-invalid @enderror" value="{{old('date')}}">
               @error('date')
                   <p class="text-danger">{{$message}}</p>
               @enderror
            </div>

            <!-- time -->
            <div class="mb-6 col-md-12">
               <label for="time">ساعت حرکت</label>
               <input type="time" name="time" id="time" class="form-control @error('time') is-invalid @enderror" value="{{old('time')}}">
               @error('time')
                   <p class="text-danger">{{$message}}</p>
               @enderror
            </div>

            <!-- capacity -->
            <div class="mb-6 col-md-12">
                <label for="capacity">ظرفیت</label>
                <input type="number" name="capacity" id="capacity" class="form-control @error('capacity') is-invalid @enderror" value="{{old('capacity')}}" placeholder= "1">
                @error('capacity')
                   <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            </br>

            <div class="mb-3 col-12">
                <div class=" mt-50">
                    <button type="submit" class=" form-control btn btn-success">ثبت</button>
                </div>
            </div>

        </form>
    </div>
</div>
@endsection