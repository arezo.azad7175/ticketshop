<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- title & icon -->
    <title>Safir</title>
    <link rel="shortcut icon" href="{{asset('img/bus_icon.jpg')}}" type="image/x-icon">
    <!-- bootstrap links -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <!-- css -->
    @vite(['resources/css/Admins/adminAuth.css'])

</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="m-2 col-12 col-lg-6 shadaw admin">
                    <h3>ورود ادمین</h3>
                    <br>
                    <!-- login-form -->
                    <form action="{{route('adminLogin.post')}}" method="post" >
                        @csrf
                        <div class="mb-6 col-md-12">
                            <input type="email" name = 'email' class="form-control" placeholder="ایمیل خود را وارد کنید" value="{{old('email')}}">
                        </div>

                        <div class="mb-6 col-md-12">
                            <input type="password" name = 'password' class="form-control" placeholder="پسورد خود را وارد کنید" >
                        </div>
                        </br>
                        <!-- show login errors -->
                        @if (Session::has('loginError'))
                            <div>
                                <p class="errors">{{Session::get('loginError')}} . </p>
                            </div>
                        @endif    
                        <!-- show send pass alert -->
                        @if (Session::has('passSend'))
                            <div>
                                <p class="info">{{Session::get('passSend')}} . </p>
                            </div>
                        @endif 

                        <p><a href="{{route('admin.Register')}}" class="backLinks">ثبت نام</a></p>
                        <p><a href="{{route('forgetPass.get')}}"  class="backLinks">فراموشی رمز عبور</a></p>

                        <div class="mb-3 col-12">
                            <div class=" mt-50">
                                <button type="submit" class=" form-control btn btn-success">ورود</button>
                            </div>
                        </div>
            
                    </form>
            </div>
        </div>
    </div>

</body>
</html>