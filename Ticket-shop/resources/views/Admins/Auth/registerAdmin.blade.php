<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- title & icon -->
    <title>Safir</title>
    <link rel="shortcut icon" href="{{asset('img/bus_icon.jpg')}}" type="image/x-icon">
    <!-- bootstrap links -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    <!-- css -->
    @vite(['resources/css/Admins/adminAuth.css'])

</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="m-2 col-12 col-lg-6 shadaw admin">
                    <h3>ثبت نام ادمین</h3>
                    <br>
                    <!-- register for with validation -->
                    <form action="{{route('adminRegister.post')}}" method="post"  enctype='multipart/form-data'>
                        @csrf
                        <!-- firstName -->
                        <div class="mb-6 col-md-12">
                            <input type="text" name = 'firstName' class="form-control  @error('firstName') is-invalid @enderror" placeholder="نام خود را وارد کنید" value="{{old('firstName')}}" >
                            @error('firstName')
                                <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <!-- lastName -->
                        <div class="mb-6 col-md-12">
                            <input type="text" name = 'lastName' class="form-control   @error('lastName') is-invalid @enderror" placeholder="نام خانوادگی خود را وارد کنید" value="{{old('lastName')}}">
                            @error('lastName')
                                <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <!-- email -->
                        <div class="mb-6 col-md-12">
                            <input type="email" name = 'email' class="form-control @error('email') is-invalid @enderror" placeholder="ایمیل خود را وارد کنید" value="{{old('email')}}">
                            @error('email')
                               <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <!-- phoneNumber -->
                        <div class="mb-6 col-md-12">
                            <input type="number" name = 'phoneNumber' class="form-control  @error('phoneNumber') is-invalid @enderror" placeholder="شماره تماس خود را وارد کنید" value="{{old('phoneNumber')}}">
                            @error('phoneNumber')
                               <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <!-- password -->
                        <div class="mb-6 col-md-12">
                            <input type="password" name = 'password' class="form-control  @error('password') is-invalid @enderror" placeholder="پسورد خود را وارد کنید" >
                            @error('password')
                               <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <!-- confirm Password -->
                        <div class="mb-6 col-md-12">
                            <input type="password" name = 'password_confirmation' class="form-control @error('confirmPassword') is-invalid @enderror" placeholder="پسورد وارد شده را تکرار کنید">
                            @error('confirmPassword')
                               <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <!-- picture -->
                        <div class="mb-6 col-md-12">
                            <input type="file" name = 'avatar' class="form-control @error('avatar') is-invalid @enderror" placeholder="عکس خود را آپلود کنید" value="{{old('avatar')}}">
                            @error('avatar')
                               <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        
                        </br>

                        <!-- go login page -->
                        <p><a href="{{route('admin.login')}}" class="backLinks"> قبلا ثبت نام کرده ام</a></p>

                        <div class="mb-3 col-12">
                            <div class=" mt-50">
                                <button type="submit" class=" form-control btn btn-success">ثبت</button>
                            </div>
                        </div>
            
                    </form>
            </div>
        </div>
    </div>
</body>
</html>