@extends('Admins.layout.master')


@section('content')

  <div class="row dasboard">
    <!-- count of reserve -->
     <div class="col-4 numberOfReservations">
        <h3>تعداد رزرو ها</h3>
        <p class="countTables">{{Session::get('orders')}}</p>
     </div>
     <!-- count of tickets -->
     <div class="col-4 numberOfTickets">
       <h3>تعدادبلیط ها</h3>
       <p class="countTables">{{Session::get('tickets')}}</p>
     </div>
     <!-- count of users -->
     <div class="col-4 numberOfUsers">
       <h3> تعداد کاربران</h3>
       <p class="countTables">{{Session::get('users')}}</p>
     </div>
  
  </div>

    
@endsection