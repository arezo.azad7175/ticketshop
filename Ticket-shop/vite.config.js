import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/Admins/adminAuth.css',
                'resources/css/Admins/admin.css',
                
                'resources/css/Users/userAuth.css',
                'resources/css/Users/user.css',
            ],
            refresh: true,
        }),
    ],
});
